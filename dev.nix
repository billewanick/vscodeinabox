###############################################################################
# This is a Nix derivation to create a shell with a VSCodium environment 
#      setup for developing JavaScript/Haskell
# VSCodium is the VSCode editor with all Microsoft labeling taken out. No telemetry, either.
# Note, some extensions won't work with the non-free editor.
# Installing extensions other than hashing below is currently a pain, be warned.
#
###############################################################################
# INSTALL NIX
###############################################################################
#
# To install Nix on Catalina, see this blog: https://dev.to/louy2/installing-nix-on-macos-catalina-2acb
# TL;DR, need to run these commands
/*
  $ curl -L https://raw.githubusercontent.com/NixOS/nix/d42ae78de9f92d60d0cd3db97216274946e8ba8e/scripts/create-darwin-volume.sh | sh
  $ sudo mdutil -i off /nix
  $ curl -L https://nixos.org/nix/install | sh
*/
# In any other system, just running this command installs Nix:
#  $ curl -L https://nixos.org/nix/install | sh
# Then restart your terminal to load the nix commands.
#
###############################################################################
# RUNNING VSCODIUM
###############################################################################
#
# In your terminal, navigate to the root folder
# To load a shell with only the necessary VSCode environment, run
#   $ nix-shell --pure dev.nix
# Nix should install everything first time, after that it's cached
# In the [nix-shell] prompt, type
#  $ codium <project>/
# Codium should then open up and the terminal is no longer needed to run.
#
###############################################################################
# ADDING SETTINGS
###############################################################################
#
# User settings don't get added in automatically, trying to work on a way to make that happen.
# It is recommended to add the following settings to make sure built-in extensions work.
# Two ways to open Settings.json
#   1. In the top menu, go to Code -> Preferences -> Settings
#      On the top right side is a button Open Settings JSON.
#  OR
#   2. Hit key-combo Command+P
#      Type in settings.json
#      Select settings.json found in ~/Library/Application Support/Code/User
# Add the following:
/* not this line
{
  "editor.formatOnSave": true,
  "editor.codeActionsOnSave": {
    "source.fixAll": true
  },
  "[javascript]": {
    "editor.defaultFormatter": "numso.prettier-standard-vscode"
  },
  "git.path": "git"
}
not this line */
#
# YOU ARE NOW FREE TO USE VSCODIUM
# DIRECT ALL FEEDBACK TO BILL EWANICK - bill@ewanick.com
#
###############################################################################
# KNOWN ISSUES
###############################################################################
# [Git credentials not being found]
#   Trying to use the UI for git doesn't get my credentials.
#   Need to find way to add these to settings.
#
# [Settings not added in automatically]
#   See the instructions has a section for adding settings.
#   Fix this issue an automate the settings injection.
# [Adding plugins quickly sucks]
#   See the instructions below on how to add extensions by getting hashes.
#   This is fine for extensions that we want to keep, but makes it very
#     time consuming to just test out extentions. Need to get local storage
#     to work correctly.
###############################################################################
{ nixpkgs ? import <nixpkgs> {} }:
let
  inherit (nixpkgs) pkgs;

  vscodium-with-extensions = pkgs.vscode-with-extensions.override {
    vscode = pkgs.vscodium;
    vscodeExtensions = extensions;
  };

  extensions = (with pkgs.vscode-extensions; [
    # Any of these: https://github.com/NixOS/nixpkgs/blob/master/pkgs/misc/vscode-extensions/default.nix
    bbenoist.Nix
  ]) ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
    # How I added extensions
    # Find your extension here: https://marketplace.visualstudio.com/vscode
    # Eg. End up on an extension page: https://marketplace.visualstudio.com/items?itemName=auchenberg.vscode-browser-preview
    # Search for "Download Extension" and save the .vsix file
    # Open a terminal, cd to the file, and generate the hash typing:
    #   shasum -a 256 auchenberg.vscode-browser-preview-0.6.7.vsix
    # Take the output and put it as the sha256 parameter: "bf7fe6e9b650e28df7b0b7d931e2ac18022ed134c5e655f2bdfe0bdcdd8bb205";
    # Use the version from the file, eg. "0.6.7";, for the version parameter.
    {
      name = "prettier-standard-vscode";
      publisher = "numso";
      version = "0.8.1";
      sha256 = "7af7bfd5f70ba493dda9cceddaf465c165c0029cf9df4465f83f1b14c4b9b6b6";
    }
    {
      name = "vscode-standardjs";
      publisher = "chenxsan";
      version = "1.3.0";
      sha256 = "c680b9676f6978e06167d2f5eaa98efed1bbee692970a3b3ea178deefb52f14f";
    }
    {
      name = "code-spell-checker";
      publisher = "streetsidesoftware";
      version = "1.9.0";
      sha256 = "231c37e1270ca6cedb8b52f04251e6dfa2396335b1f648219945c94bf6fa8045";
    }
  ];

  nixPackages = [
    # These ones required for VSCodium
    vscodium-with-extensions
    pkgs.python
    pkgs.git

    # Search for dependencies here:
    # https://nixos.org/nixos/packages.html?channel=nixos-20.03
    pkgs.nodejs-12_x
  ];
in
pkgs.stdenv.mkDerivation {
  name = "env";
  buildInputs = nixPackages;
}